import sqlite3
import pets.schemas as schemas
from datetime import datetime

database_name : str = "PetsDb.db"


def add_pet(new_pet : schemas.PetIn):
    connection = sqlite3.connect(database_name)
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()
    current_date = datetime.now().isoformat().split(".")[0]
    cursor.execute("INSERT INTO pets (name, age, type, creation_date) VALUES (?, ?, ?, ?)",
                   (new_pet.name, new_pet.age, new_pet.type.name, current_date))
    connection.commit()
    resp = cursor.execute("SELECT * FROM pets ORDER BY id DESC LIMIT 1").fetchone()
    connection.close()
    return schemas.PetOut(id = resp["id"], name = resp["name"], age = resp["age"],
                          type = schemas.PetType(resp["type"]), created_at = resp["creation_date"])

def get_pets(limit : int):
    connection = sqlite3.connect(database_name)
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()
    items = cursor.execute("SELECT * FROM pets ORDER BY id LIMIT ?",(limit,)).fetchall()
    connection.commit()
    connection.close()
    items = [schemas.PetOut(id = i["id"], name = i["name"], age = i["age"],
                            type = schemas.PetType(i["type"]), created_at = i["creation_date"]) for i in items]
    pets = schemas.PetsGetOut(count = len(items), items = items)
    return pets
def delete_pets(ids : list[int]):
    connection = sqlite3.connect(database_name)
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()
    all_pets_ids = cursor.execute("SELECT id FROM pets ").fetchall()
    all_pets_ids = [ i["id"] for i in all_pets_ids]
    deleted_pets = schemas.PetsDeleteOut(deleted = 0, errors = [])
    for i in ids:
        if i in all_pets_ids:
            cursor.execute("DELETE FROM pets WHERE id = ?",(i,))
            deleted_pets.deleted += 1
        else:
            deleted_pets.errors.append(schemas.PetsDeleteOutError(id = i,
                                                                  error ="Pet with the matching ID was not found."))
    connection.commit()
    connection.close()
    return deleted_pets