from enum import Enum
from pydantic import BaseModel, Field


class PetType(Enum):
    cat = "cat"
    dog = "dog"
    crocodile = "crocodile"
    horse = "horse"
    parrot = "parrot"


class PetOut(BaseModel):
    id : int 
    name : str
    age : int = Field(ge=0)
    type : PetType
    created_at : str


class PetIn(BaseModel):
    name : str
    age : int = Field(ge=0)
    type : PetType


class PetsGetOut(BaseModel):
    count : int
    items : list[PetOut]


class PetsDeleteOutError(BaseModel):
    id : int
    error : str


class PetsDeleteOut(BaseModel):
    deleted : int
    errors : list[PetsDeleteOutError]

