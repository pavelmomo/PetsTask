from fastapi import APIRouter
import pets.schemas as pets_schemas
import pets.service as PetsService
from fastapi import Body


pets_router = APIRouter( prefix="/pets", tags=["Pets"])


@pets_router.get("", response_model= pets_schemas.PetsGetOut)
def get_pets(limit : int = 20):
    response = PetsService.get_pets(limit)
    return response

@pets_router.post("", response_model = pets_schemas.PetOut)
def add_pet(new_pet : pets_schemas.PetIn):
    response = PetsService.add_pet(new_pet)
    return response

@pets_router.delete("", response_model = pets_schemas.PetsDeleteOut)
def delete_pets(ids : list[int] = Body(embed=True)):
    response = PetsService.delete_pets(ids)
    return response