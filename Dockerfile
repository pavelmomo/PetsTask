FROM python:3.11-alpine
COPY . /usr/src/PetsApp
WORKDIR /usr/src/PetsApp
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 3000
CMD python3 main.py
