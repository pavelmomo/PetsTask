import uvicorn
from fastapi.responses import HTMLResponse
from pets.router import  pets_router
from fastapi import FastAPI

app = FastAPI()
app.include_router(pets_router)


@app.get("/")
def index():
    return HTMLResponse('<h1 align="center">Добро пожаловать!<h1>')


if (__name__ == "__main__"):
    uvicorn.run(app=app,host= "0.0.0.0" ,port = 3000)
